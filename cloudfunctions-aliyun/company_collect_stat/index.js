'use strict';
const db = uniCloud.database()
const dbCmd = db.command
const diffTime = 8 * 60 * 60 * 1000;
// 获取指定的今天的时间戳
function getMsByDate(timeStr, dayStr) {
	return new Date(dayStr+' '+timeStr).getTime()-diffTime;
}
exports.main = async (event, context) => {
	const $ = db.command.aggregate
	const collection = db.collection('company_collect');
	let todayStart = getMsByDate('00:00:00',event.day);
	let todayEnd = getMsByDate('23:59:59',event.day);
	let whereMap = {
		'company':event.company,
		'createTime': dbCmd.gte(todayStart).and(dbCmd.lte(todayEnd))
	};
	const res = await collection
		.aggregate()
		.match(whereMap)
		.group({
			_id: '$advice',
			data: $.sum(1)
		})
		.end();
	return {
		data: res.data || []
	};
};
