const moment = require('moment');
const {radioTxt,adviceTxt} = require('./hvl-const.js')
const excelConfig = {
	// 公司员工打卡下载
	'companyCollect': {
		'funcName': 'company_collect_excel', //请求云函数名
		'downloadName':'员工健康打卡表',
		//字段定义
		'showFields': {
			'company': "企业号",
			'department': "部门",
			'jobnumber': "工号",
			'name': "姓名",
			'questionA': {
				title: "在过去的14天内，是否出过本地区",
				//自定义回调函数
				callback: value => {
					return radioTxt[value];
				}
			},
			'questionB': {
				title: "在过去的14天内，是否到过疫情高发区",
				//自定义回调函数
				callback: value => {
					return radioTxt[value];
				}
			},
			'questionC': {
				title: "在过去的14天内，是否与疫情高发区旅居人员接触",
				//自定义回调函数
				callback: value => {
					return radioTxt[value];
				}
			},
			'questionD': {
				title: "是否与疑似病例接触",
				//自定义回调函数
				callback: value => {
					return radioTxt[value];
				}
			},
			'questionE': {
				title: "在过去的14天内，是否经停疫情高发区",
				//自定义回调函数
				callback: value => {
					return radioTxt[value];
				}
			},
			'questionF': {
				title: "在过去的14天内，是否在外地乘坐公共交通工具（包括：火车，飞机，汽车等）",
				//自定义回调函数
				callback: value => {
					return radioTxt[value];
				}
			},
			'questionG': {
				title: "在过去的14天内，是否有发热，呼吸道症状等",
				//自定义回调函数
				callback: value => {
					return radioTxt[value];
				}
			},
			'advice': {
				title: "筛查结果",
				//自定义回调函数
				callback: value => {
					return adviceTxt[value];
				}
			},
			'extraInfo': "其他情况说明",
			'createTime': {
				title: "提交时间",
				//自定义回调函数
				callback: value => {
					return moment(value).format('YYYY-MM-DD HH:mm:ss');
				}
			}
		}
	},
	// 企业下载
	'companyAccount': {
		'funcName': 'company_account_excel',
		'downloadName':'企业注册信息表',
		'showFields': {
			company_no: '企业号',
			company_name: '名称',
			email: '邮箱',
			phone: '手机',
			createTime: {
				title: "提交时间",
				//自定义回调函数
				callback: value => {
					return value ? moment(value).format('YYYY-MM-DD HH:mm:ss') : '';
				}
			}
		}
	},
	// 商业合作
	'companyCooperate':{
		'funcName': 'company_cooperate_excel', //请求云函数名
		'downloadName':'商业合作信息表',
		'showFields':{
			nick:'称呼',
			phone:'手机',
			email:'邮箱',
			content:'合作描述',
			createTime: {
				title: "提交时间",
				//自定义回调函数
				callback: value => {
					return value ? moment(value).format('YYYY-MM-DD HH:mm:ss') : '';
				}
			}
		}
	},
	// 建议反馈
	'companySuggest':{
		'funcName': 'company_suggest_excel', //请求云函数名
		'downloadName':'意见反馈信息表',
		'showFields':{
			nick:'称呼',
			phone:'手机',
			email:'邮箱',
			content:'问题描述',
			createTime: {
				title: "提交时间",
				//自定义回调函数
				callback: value => {
					return value ? moment(value).format('YYYY-MM-DD HH:mm:ss') : '';
				}
			}
		}
	}
};
module.exports = {
	excelConfig
}