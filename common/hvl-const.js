const radioTxt = {
	1: '是',
	0: '否'
}
const adviceTxt = {
	1: '可以直接上班',
	2: '专家判断是否需要隔离',
	3: '居家或到当地指定地点隔离14天',
	4: '去定点发烧门诊就诊',
	5: '常规感冒治疗'
}
module.exports = {
	radioTxt,
	adviceTxt
}